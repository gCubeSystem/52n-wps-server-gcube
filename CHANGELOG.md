
# Changelog


## [v3.6.3-SNAPSHOT] - 2020-11-12

### Features

- Updated to Git/Jenkins [#20101]


## [v3.6.1] - 2017-07-05

### Features

- First Release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
