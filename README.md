# 52n WPS Server gCube

This is a library to support DataMiner service that integrates the WPS 52North in the D4Science Infrastructure.

## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* Use of this library is described on [Wiki](https://wiki.gcube-system.org/gcube/DataMiner).

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/52n-wps-server-gcube/releases).

## Authors

* **Lucio Lelii** ([ORCID]()) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)


